export class PokemonsArr {
  url: string;
  name: string;
}

export class Pokemon {
  forms: [
    {
      url: string,
      name: string
    }
  ];
  abilities: [
    {
      slot: number,
      is_hidden: boolean,
      ability: {
        url: string,
        name: string
      }
    }
  ];
  name: string;
  weight: number;
  moves: [any];
  sprites: {
    back_female: string,
    back_shiny_female: string,
    back_default: string,
    front_female: string,
    front_shiny_female: string,
    back_shiny: string,
    front_default: string,
    front_shiny: string
  };
  held_items: [any];
  location_area_encounters: string;
  height: 3;
  is_default: boolean;
  species: {
    url: string,
    name: string
  };
  id: 16;
  order: 21;
  game_indices: [any];
  base_experience: 50;
  types: [
    {
      slot: number,
      type: {
        url: string,
        name: string
      }
    },
    {
      slot: number,
      type: {
        url: string,
        name: string
      }
    }
  ]
}