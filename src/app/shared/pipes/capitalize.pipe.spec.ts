import { CapitalizePipe } from './capitalize.pipe';

describe('CapitalizePipe', () => {
  it('should return the first letter as uppercase', () => {
    const pipe = new CapitalizePipe();
    const transformation = pipe.transform('exit');
    expect(transformation.charAt(0)).toBe('E');
  });
  
  it('should return the letters after the first as lowercase', () => {
    const pipe = new CapitalizePipe();
    const transformation = pipe.transform('exit');
    expect(transformation.charAt(1)).toBe('x');
    expect(transformation.charAt(2)).toBe('i');
    expect(transformation.charAt(3)).toBe('t');
  });
});
