import { HeightPipe } from './height.pipe';

describe('HeightPipe', () => {
  it('should return the passed height times 100 with a "cm" string appended' , () => {
    const pipe = new HeightPipe();
    expect(pipe.transform(96)).toBe('9600 cm');
  });
});
