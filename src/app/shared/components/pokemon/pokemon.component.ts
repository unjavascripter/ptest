import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../../core/services/data.service';
import { Pokemon } from '../../../shared/pokemon.model';

@Component({
  selector: 'pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {

  @Input() name;
  public pokemon: Pokemon;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getOne(this.name).subscribe((pokemon: Pokemon) => {
      console.log(pokemon);
      this.pokemon = pokemon;
    });
  }

}
