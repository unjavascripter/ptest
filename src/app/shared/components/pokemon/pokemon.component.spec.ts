import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonComponent } from './pokemon.component';
import { CapitalizePipe } from '../../pipes/capitalize.pipe';
import { HeightPipe } from '../../pipes/height.pipe';
import { DataService } from '../../../core/services/data.service';
import { Observable } from 'rxjs/Observable';

describe('PokemonComponent, should create a pokemon component with the data provided by the api', () => {
  let component: PokemonComponent;
  let fixture: ComponentFixture<PokemonComponent>;
  let dataService;
  let DataServiceStub = {
    getOne: () => new Observable(observer => {
      observer.next({name: 'unknown', height: '96', types:[{type: {name: 'all'}}], sprites: {front_default: 'url/img.bmp'}})
      observer.complete();
    })
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonComponent, CapitalizePipe, HeightPipe ],
      providers:    [ {provide: DataService, useValue: DataServiceStub } ]
    })
    .compileComponents();
    dataService = TestBed.get(DataService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('the name in the DOM should match the api data afer passing through the capitalize filter', () => {
    component.name = 'unknown';
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.name').textContent).toContain('Unknown');
  });
  
  it('the types in the DOM should match the api data afer passing through the capitalize filter', () => {
    component.name = 'unknown';
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.types').textContent).toContain('All');
  });
  
  it('the height in the DOM should match the api data afer passing through the height filter', () => {
    component.name = 'unknown';
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.height').textContent).toContain('Height: 9600 cm');
  });
});
