import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { HeightPipe } from './pipes/height.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PokemonComponent,
    CapitalizePipe,
    HeightPipe
  ],
  exports: [
    PokemonComponent,
    CapitalizePipe,
    HeightPipe
  ]
})
export class SharedModule { }
