import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { PokemonsArr, Pokemon } from '../../shared/pokemon.model';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
  protected API_URL = 'http://pokeapi.co/api/v2';

  constructor(private http: Http) {}

  getPokemons(howMany: number = 20) {
    return this.http.get(`${this.API_URL}/pokemon`, {search: String(howMany)}).map(response => response.json().results as PokemonsArr[]);
    
  }

  getOne(name) {
    return this.http.get(`${this.API_URL}/pokemon/${name}`).map(response => response.json() as Pokemon);
  }

}
