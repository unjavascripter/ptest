import { TestBed, inject } from '@angular/core/testing';
import {
  HttpModule,
  Http,
  Response,
  ResponseOptions,
  XHRBackend,
  BaseRequestOptions
} from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { DataService } from './data.service';

describe('DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        DataService
      ],
    });
  });

  it('should return an array of pokemons when calling getPokemons()', inject([DataService], (service: DataService) => {
    const spy = spyOn(service, 'getPokemons').and.returnValue(JSON.stringify([{name: 'unknown', height: 588}, {name: 'mew', height: 50}]));
    service.getPokemons();
    expect(JSON.parse(spy.calls.mostRecent().returnValue)[0].name).toBe('unknown');
    expect(JSON.parse(spy.calls.mostRecent().returnValue)[1].height).toBe(50);
  }));

  it('should return a pokemon object when calling getOn()', inject([DataService], (service: DataService) => {
    const spy = spyOn(service, 'getOne').and.returnValue(JSON.stringify({name: 'unknown', height: 588}));
    service.getOne('unknow');
    expect(spy.calls.mostRecent().returnValue).toBe(JSON.stringify({name: 'unknown', height: 588}));
  }));
  

});
