import { Component } from '@angular/core';
import { Response } from '@angular/http';
import { DataService } from './core/services/data.service';
import { PokemonsArr } from './shared/pokemon.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public pokemonsArr: PokemonsArr[];

  constructor(private dataService: DataService){
    this.dataService.getPokemons().subscribe((pokemonsArr: PokemonsArr[]) => {
      this.pokemonsArr = pokemonsArr;
    });
  }

}
