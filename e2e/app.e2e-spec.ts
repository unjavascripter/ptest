import { BlablablAppPage } from './app.po';

describe('blablabl-app App', () => {
  let page: BlablablAppPage;

  beforeEach(() => {
    page = new BlablablAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
