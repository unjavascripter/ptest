# Archivos listos para producción

Se encuentran en la carpeta `./dist`, cualquier servidor HTTP podrá servir los archivos (Apache, NGINX, IIS...).


# Archivos de desarrollo

## Requisitos

- Node v6.9.0^

> Para verlos en acción debe instalarse Angular-cli:
- `npm install -g @angular/cli`

## Instalación de dependencias

- `npm i`

## Pruebas unitarias

Una vez instalado angular-cli y las dependencias del proyecto se pueden poner a andar las pruebas unitarias.

- `ng test`
